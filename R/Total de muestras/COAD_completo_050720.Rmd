---
title: "Análisis de COAD todas las muestras"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

# CARGA DE BIBLIOTECAS Y DATOS

#### Instalación de todos los paquetes que vamos a utilizar durante el análisis.

```{r}
list.of.packages <- c("ggplot2", "dplyr", "fastDummies", "faraway", "tidyverse", "rlang", "cowplot", "caret", "lattice",
                      "e1071", "glmnet", "Matrix", "caretEnsemble", "GGally", "rpart", "randomForest", "naivebayes",
                      "BiocManager", "MLmetrics", "mlbench", "parallel", "doParallel", "funModeling", "GA", "corrplot")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

```{r}
library(GGally)
library(fastDummies)
library(ggplot2)
library(dplyr)
library(faraway)
library(tidyverse)
library(rlang)
library(cowplot)
library(corrplot)
```


#### Carga de los datos a analizar

```{r}
datos_COAD_completo <- read.csv('C:\\Users\\Laura\\Desktop\\TFM Total muestras\\COAD\\datos_COAD_completo.csv', sep = ',', encoding = 'UTF-8')

datos_COAD_completo$Muestra <- NULL
head(datos_COAD_completo)

datos_COAD_mat <- as.matrix(datos_COAD_completo)
```
```{r}
summary(datos_COAD_completo)
```


# REPRESENTACIÓN GRÁFICA DE LOS DATOS

#### Conjunto de los datos

```{r}
ggplot(datos_COAD_completo, aes(x = Cáncer, fill = Cáncer) ) + geom_bar() + scale_fill_manual(values = 
                                                                                                c('#4d648d', '#d0e1f9'))
```


#### Expresión génica en cajas

```{r}
plot_for_loop <- function(df, .x_var, .y_var) {
  
  # convert strings to variable
  x_var <- sym(.x_var)
  y_var <- sym(.y_var)
  
  # unquote variables using !! 
  ggplot(df, aes(x= !! x_var, y = !! y_var, fill = !! x_var)) + 
    geom_boxplot() + 
    labs(x = x_var, y = y_var) +
    theme(axis.text = element_text(size = 10), axis.title = element_text(size = 15), legend.key.size = unit(1, 'cm')) + 
    scale_fill_manual(values = c('#4d648d', '#d0e1f9'))
}
```

```{r}
plot_list <- colnames(datos_COAD_completo)[-1] %>% 
  map( ~ plot_for_loop(datos_COAD_completo, colnames(datos_COAD_completo)[1], .x))
```

```{r}
pdf('plot_list_Completo.pdf', height = 10, width = 10)

plot_list

dev.off()
```


# ESTUDIO PREVIO DE VARIABLES PREDICTORAS

#### Creación de variables DUMMYS

```{r}
datos_COAD_dum <- dummy_cols(datos_COAD_completo, select_columns = c('Cáncer'))

datos_COAD_dum$Cáncer <- NULL
datos_COAD_dum$Cáncer_Otro <- NULL

datos_COAD_dum <- select(datos_COAD_dum, Cáncer_COAD, everything())
head(datos_COAD_dum)

COAD_genes <- select(datos_COAD_dum, -(Cáncer_COAD))
```


#### Estudio correlación

Análisis de la correlación en tabla

```{r}
relacion_gen <- cor(COAD_genes)
print(relacion_gen)
```


```{r}
corrplot(cor(COAD_genes), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .3, cl.cex = .8)
```

Representación gráfica de la correlación

```{r}
cor_completa <- cor(COAD_genes)
ord <- corrMatOrder(cor_completa, order="FPC")
cor_dibujo <- cor_completa[ord, ord]

pdf('plot_corr_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = .5, cl.cex = 5)

dev.off()
```


# SELECCIÓN DE VARIABLES PREDICTORAS.

#### Seleción stepAIC

```{r}
fit1 <- glm(Cáncer_COAD~., data = datos_COAD_dum, family = 'binomial')
fit0 <- glm(Cáncer_COAD~1, data = datos_COAD_dum, family = 'binomial')
```

```{r}
install.packages("MASS")
library(MASS)

step_both <- stepAIC (fit0, direction = 'both', scope=list(upper=fit1, lower=fit0))
```

```{r}
summary(step_both)
```


```{r}
coefficients(step_both)
```


```{r}
COAD_X <- (datos_COAD_dum[,2:99])

COAD_Y <- (datos_COAD_dum[,1])
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
y_predicc_both <- as.numeric(predict(step_both, COAD_X, type = 'response')>.5)

matriz_both <- confusionMatrix(as.factor(COAD_Y), as.factor(y_predicc_both), mode = 'everything')

matriz_both
```

Estas métricas nos hacen intuir que existe colinealidad, para comprobarlo se repite el proceso sobre train y test

```{r}
COAD_X_train <- datos_COAD_dum[1:640, 2:99]
COAD_X_test <- datos_COAD_dum[641:801, 2:99]

COAD_Y_train <- datos_COAD_dum[1:640, 1]
COAD_Y_test <- datos_COAD_dum[641:801, 1]
```

```{r}
y_predicc_train <- as.numeric(predict(step_both, COAD_X_train, type = 'response')>.5)

matriz_both_train <- confusionMatrix(as.factor(COAD_Y_train), as.factor(y_predicc_train), mode = 'everything')

matriz_both_train
```

```{r}
y_predicc_test <- as.numeric(predict(step_both, COAD_X_test, type = 'response')>.5)

matriz_both_test <- confusionMatrix(as.factor(COAD_Y_test), as.factor(y_predicc_test), mode = 'everything')

matriz_both_test
```

LOS DATOS OBTENIDOS DE LA MATRIZ PARA TRAIN Y TEST NOS INDICAN LA EXISTENCIA DE OVERFITTING, PARA EVITARLA PROBAREMOS CON TRES TIPOS DE SELECCIÓN DE VARIABLES: 1. LA OBTENIDA POR EL MENOR AIC, 2. LA OBTENIDA POR EL SEGUNDO MENOR AIC Y 3. LA OBTENIDA POR LASSO.


#### SELECCIÓN CON LASSO

```{r}
COAD_X_mat <- data.matrix(COAD_X)

COAD_Y_mat <- data.matrix(COAD_Y)

library(glmnet)

set.seed(999)

COAD_lasso <- cv.glmnet(COAD_X_mat, COAD_Y_mat, family = 'binomial', alpha = 1, parallel = TRUE, standardize = TRUE, 
                        type.measure = 'auc')
plot(COAD_lasso)
```

```{r}
COAD_lasso$lambda.min
```

```{r}
coef(COAD_lasso, s = COAD_lasso$lambda.min)
```

```{r}
y_predicc_LASSO <- as.numeric(predict.glmnet(COAD_lasso$glmnet.fit, newx = COAD_X_mat, s = COAD_lasso$lambda.min)>.5)

matrix_LASSO <- confusionMatrix(as.factor(COAD_Y_mat), as.factor(y_predicc_LASSO), mode = 'everything')
matrix_LASSO
```



# CREACIÓN DE SUBCONJUNTOS

#### VARIABLES PREDICTORAS AIC

```{r}
detach(package: MASS)

var_AIC <- select(datos_COAD_dum, Cáncer_COAD, gene_4874, gene_346, gene_2537, gene_19805, gene_4053, gene_15024,
                        gene_957, gene_14112)

var_AIC_X_train <- var_AIC [1:640, 2:9]
var_AIC_X_test <- var_AIC [641:801, 2:9]

var_AIC_Y_train <- var_AIC [1:640, 1]
var_AIC_Y_test <- var_AIC [641:801, 1]

var_AIC_cor <- select(var_AIC, -(Cáncer_COAD))
```

Estudio de la correlación para variables predictoras AIC

```{r}
corrplot(cor(var_AIC_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .6, cl.cex = .8)
```

```{r}
cor_completa_AIC_FPC <- cor(var_AIC_cor)
ord <- corrMatOrder(cor_completa_AIC_FPC, order="FPC")
cor_dibujo_AIC_FPC <- cor_completa_AIC_FPC[ord, ord]

pdf('plot_corr_AIC_FPC_dos.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_AIC_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 3, cl.cex = 5)

dev.off()
```
```{r}
write.csv(var_AIC, file = 'variables_AIC_COAD801.csv', row.names = FALSE)
```

#### VARIABLES CON COLINEALIDAD, SEGUNDO MENOR AIC

```{r}
var_overf <- select(datos_COAD_dum, Cáncer_COAD, gene_4874, gene_346, gene_2537, gene_19805, gene_12120, gene_4053, 
                    gene_15024, gene_957, gene_14112)

var_overf_X_train <- var_overf[1:640, 2:10]
var_overf_X_test <- var_overf[641:801, 2:10]

var_overf_Y_train <- var_overf[1:640, 1]
var_overf_Y_test <- var_overf[641:801, 1]

var_overf_cor <- select(var_overf, -(Cáncer_COAD))
```


```{r}
corrplot(cor(var_overf_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .7, cl.cex = .8)
```


```{r}
cor_completa_OVER_FPC <- cor(var_overf_cor)
ord <- corrMatOrder(cor_completa_OVER_FPC, order="FPC")
cor_dibujo_OVER_FPC <- cor_completa_OVER_FPC[ord, ord]

pdf('plot_corr_OVER_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_OVER_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 2, cl.cex = 5)

dev.off()
```

```{r}
write.csv(var_overf, file = 'variables_OVER_COAD801.csv', row.names = FALSE)
```

#### VARIABLES CON COLINEALIDAD, TERCER MENOR AIC

```{r}
var_overf_tres <- select(datos_COAD_dum, Cáncer_COAD, gene_4874, gene_346, gene_2537, gene_19805, gene_12120, gene_4053, 
                    gene_15024, gene_957)

tres_overf_X_train <- var_overf_tres[1:640, 2:9]
tres_overf_X_test <- var_overf_tres[641:801, 2:9]

tres_overf_Y_train <- var_overf_tres[1:640, 1]
tres_overf_Y_test <- var_overf_tres[641:801, 1]

tres_overf_cor <- select(var_overf_tres, -(Cáncer_COAD))
```


```{r}
corrplot(cor(tres_overf_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .7, cl.cex = .8)
```


```{r}
cor_completa_OVER_FPC <- cor(tres_overf_cor)
ord <- corrMatOrder(cor_completa_OVER_FPC, order="FPC")
cor_dibujo_OVER_FPC <- cor_completa_OVER_FPC[ord, ord]

pdf('plot_corr_tres_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_OVER_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 2, cl.cex = 5)

dev.off()
```
```{r}
write.csv(var_overf_tres, file = 'variables_TRES_COAD801.csv', row.names = FALSE)
```


#### VARIABLES LASSO

```{r}
##detach(package: MASS)

var_LASSO <- select(datos_COAD_dum, Cáncer_COAD, gene_346, gene_595, gene_682, gene_957, gene_1538, gene_1762, gene_2537,
                          gene_3115, gene_3166, gene_3436, gene_4053, gene_4292, gene_4874, gene_7551, gene_7511, gene_8162,
                          gene_8180, gene_8415, gene_9813, gene_11092, gene_11341, gene_11704, gene_11744, gene_12120,
                          gene_13289, gene_13436, gene_13977, gene_14112, gene_15009, gene_15024, gene_15652, gene_17079,
                          gene_18138, gene_18811, gene_19805)


var_LASSO_X_train <- data.matrix(var_LASSO [1:640, 2:36])
LASSO_X_train <- (var_LASSO [1:640, 2:36])
var_LASSO_X_test <- data.matrix(var_LASSO [641:801, 2:36])
LASSO_X_test <- (var_LASSO [641:801, 2:36])

var_LASSO_Y_train <- data.matrix(var_LASSO [1:640,1])
LASSO_Y_train <- (var_LASSO [1:640,1])
var_LASSO_Y_test <- data.matrix(var_LASSO [641:801, 1])
LASSO_Y_test <- (var_LASSO [641:801, 1])

var_LASSO_cor <- select(var_LASSO, -(Cáncer_COAD))
```

```{r}
corrplot(cor(var_LASSO_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .6, cl.cex = .8)
```

```{r}
cor_completa_LASSO_FPC <- cor(var_LASSO_cor)
ord <- corrMatOrder(cor_completa_OVER_FPC, order="FPC")
cor_dibujo_LASSO_FPC <- cor_completa_LASSO_FPC[ord, ord]

pdf('plot_corr_LASSO_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_LASSO_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 3, cl.cex = 5)

dev.off()
```

```{r}
write.csv(var_LASSO, file = 'variables_LASSO_COAD801.csv', row.names = FALSE)
```



# ENTRENAMIENTO DE REGRESIÓN LOGÍSTICA

#### VARIABLES AIC

```{r}
mod_STEPWISE <- glm(Cáncer_COAD~., data = var_AIC, family = 'binomial')
```

```{r}
summary(mod_STEPWISE)
```

```{r}
y_pred_STEPWISE_dos <- as.numeric(predict(mod_STEPWISE, var_AIC_X_test, type = 'response')> .5)
head(y_pred_STEPWISE_dos)
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
matrix_STEPWISE <- confusionMatrix(as.factor(y_pred_STEPWISE_dos), as.factor(var_AIC_Y_test), mode = 'everything')
matrix_STEPWISE
```

CONCLUSIÓN: LAS VARIABLES PRESENTA COLINEALIDAD POR ESTE MOTIVO NINGUNA DE ELLAS PRESENTA UN p-value SIGNIFICATIVO

```{r}
coef(mod_STEPWISE)
```

```{r}
exp(coef(mod_STEPWISE))
```


```{r}
exp(confint(mod_STEPWISE))
```

```{r}
coef_AIC_COAD801 <- as.data.frame(mod_STEPWISE$coefficients)
coef_AIC_COAD801 <- (coef_AIC_COAD801[2:9,1])

genes_AIC <- data.frame(t(var_AIC[-1]))

exportar_AIC <- cbind(genes_AIC, coef_AIC_COAD801)
exportar_AIC <- select(exportar_AIC, (coef_AIC_COAD801))

write.csv(exportar_AIC, 'coef_AIC_COAD801.csv')
```


#### VARIABLES CON SEGUNDO MENOR AIC

```{r}
mod_var_over <- glm(Cáncer_COAD~., data = var_overf, family = 'binomial')
```

```{r}
summary(mod_var_over)
```

```{r}
y_pred_over <- as.numeric(predict(mod_var_over, var_overf_X_test, type = 'response')> .5)
head(y_pred_over)
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
matrix_over <- confusionMatrix(as.factor(y_pred_over), as.factor(var_overf_Y_test), mode = 'everything')
matrix_over
```

EN ESTE CASO PARECE QUE NO DESAPARECE EL OVERFITTING

```{r}
coef(mod_var_over)
```

```{r}
exp(coef(mod_var_over))
```

```{r}
exp(confint(mod_var_over))
```

```{r}
coef_OVER_COAD801 <- as.data.frame(mod_var_over$coefficients)
coef_OVER_COAD801 <- (coef_OVER_COAD801[2:10,1])

genes_OVER <- data.frame(t(var_overf[-1]))

exportar_OVER <- cbind(genes_OVER, coef_OVER_COAD801)
exportar_OVER <- select(exportar_OVER, (coef_OVER_COAD801))

write.csv(exportar_OVER, 'coef_OVER_COAD801.CSV')
```


#### VARIABLES CON TERCER MENOR AIC

```{r}
mod_var_tres <- glm(Cáncer_COAD~., data = var_overf_tres, family = 'binomial')
```

```{r}
summary(mod_var_tres)
```

```{r}
y_pred_tres <- as.numeric(predict(mod_var_tres, tres_overf_X_test, type = 'response')> .5)
head(y_pred_tres)
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
matrix_tres <- confusionMatrix(as.factor(y_pred_tres), as.factor(tres_overf_Y_test), mode = 'everything')
matrix_tres
```

EN ESTE CASO PARECE QUE NO DESAPARECE EL OVERFITTING

```{r}
coef(mod_var_tres)
```

```{r}
exp(coef(mod_var_tres))
```

```{r}
exp(confint(mod_var_tres))
```

```{r}
coef_TRES_COAD801 <- as.data.frame(mod_var_tres$coefficients)
coef_TRES_COAD801 <- (coef_TRES_COAD801[2:9,1])

genes_TRES <- data.frame(t(var_overf_tres[-1]))

exportar_TRES <- cbind(genes_TRES, coef_TRES_COAD801)
exportar_TRES <- select(exportar_TRES, (coef_TRES_COAD801))

write.csv(exportar_TRES, 'coef_TRES_COAD801')
```


#### VARIABLES PREDICTORAS LASSO

```{r}
logit_LASSO <- cv.glmnet(var_LASSO_X_train, var_LASSO_Y_train, family = 'binomial', alpha = 1, 
                         parallel =TRUE,
                         standardize = TRUE, type.measure = 'auc')

plot(logit_LASSO)
```

```{r}
logit_LASSO$lambda.min
```


```{r}
y_predicc_LASSO <- as.numeric(predict.glmnet(logit_LASSO$glmnet.fit, newx = var_LASSO_X_test, s =
                                               logit_LASSO$lambda.min)>.5)

y_predicc_LASSO
```

```{r}
matrix_LASSO <- confusionMatrix(as.factor(var_LASSO_Y_test), as.factor(y_predicc_LASSO), mode = 'everything')
matrix_LASSO
```

```{r}
logit_LASSO$lambda.min
coef(logit_LASSO, s = logit_LASSO$lambda.min)
```

```{r}
coef_LASSO_COAD801 <- as.numeric(coef(logit_LASSO, s = logit_LASSO$lambda.min))
coef_LASSO_COAD801 <- as.data.frame(coef_LASSO_COAD801)
coef_LASSO_COAD801 <- (coef_LASSO_COAD801[2:36,1])

genes_LASSO <- data.frame(t(var_LASSO[-1]))

exportar_LASSO <- cbind(genes_LASSO, coef_LASSO_COAD801)
exportar_LASSO <- select(exportar_LASSO, (coef_LASSO_COAD801))

write.csv(exportar_LASSO, 'coef_LASSO_COAD801.csv')
```

CONCLUSIÓN: EL MODELO DE REGRESIÓN LOGÍSTICA PRESENTA COLINEALIAD CUANDO UTILIZAMOS LA SELECCIÓN DE VARIABLES PREDICTORAS OFRECIDAS POR stepAIC. SIN EMBARGO, CUANDO SE UTILIZA LA SELECCIÓN DE LASSO EL MODELO TIENE UNA ALTA EFICACIA



# MODELO NAIVE BAYES

```{r}
library(caretEnsemble)
library(GGally)
library(rpart)
library(randomForest)
library(naivebayes)
```


#### VARIABLES PREDICTARAS AIC

```{r}
DATOS_AIC <- select(datos_COAD_completo, Cáncer, gene_4874, gene_346, gene_2537, gene_19805, gene_4053, gene_15024,
                        gene_957, gene_14112)

AIC_trainData <- DATOS_AIC[1:640,]
AIC_testData <- DATOS_AIC[641:801,]
```


```{r}
modelo_NB_AIC <- naive_bayes(Cáncer~., data = AIC_trainData)
modelo_NB_AIC$tables
```

```{r}
summary(modelo_NB_AIC)
```


```{r}
pred_NB <- predict(modelo_NB_AIC, AIC_testData)
head(pred_NB)
```

```{r}
matriz_NB_AIC <- confusionMatrix(as.factor(AIC_testData$Cáncer), as.factor(pred_NB))
matriz_NB_AIC
```


#### VARIABLES PREDICTORAS OVER

```{r}
datos_over <- select(datos_COAD_completo, Cáncer, gene_4874, gene_346, gene_2537, gene_19805, gene_12120, gene_4053, 
                    gene_15024, gene_957, gene_14112)

OVER_trainData <- datos_over[1:640,]
OVER_testData <- datos_over[641:801,]
```


```{r}
modelo_NB_OVER <- naive_bayes(Cáncer~., data = OVER_trainData)
modelo_NB_OVER$tables
```

```{r}
summary(modelo_NB_OVER)
```

```{r}
pred_NB_OVER <- predict(modelo_NB_OVER, OVER_testData)
head(pred_NB_OVER)
```

```{r}
matriz_NB_OVER <- confusionMatrix(as.factor(OVER_testData$Cáncer), as.factor(pred_NB_OVER))
matriz_NB_OVER
```


#### VARIABLES PREDICTORAS OVER

```{r}
datos_tres <- select(datos_COAD_completo, Cáncer, gene_4874, gene_346, gene_2537, gene_19805, gene_12120, gene_4053, 
                    gene_15024, gene_957)

TRES_trainData <- datos_tres[1:640,]
TRES_testData <- datos_tres[641:801,]
```


```{r}
modelo_NB_TRES <- naive_bayes(Cáncer~., data = TRES_trainData)
modelo_NB_TRES$tables
```

```{r}
summary(modelo_NB_TRES)
```

```{r}
pred_NB_TRES <- predict(modelo_NB_TRES, TRES_testData)
head(pred_NB_TRES)
```

```{r}
matriz_NB_TRES <- confusionMatrix(as.factor(TRES_testData$Cáncer), as.factor(pred_NB_TRES))
matriz_NB_TRES
```


#### VARIABLES PREDICTORAS LASSO

```{r}
DATOS_LASSO <- select(datos_COAD_completo, Cáncer, gene_346, gene_595, gene_682, gene_957, gene_1538, gene_1762, gene_2537,
                          gene_3115, gene_3166, gene_3436, gene_4053, gene_4292, gene_4874, gene_7551, gene_7511, gene_8162,
                          gene_8180, gene_8415, gene_9813, gene_11092, gene_11341, gene_11704, gene_11744, gene_12120,
                          gene_13289, gene_13436, gene_13977, gene_14112, gene_15009, gene_15024, gene_15652, gene_17079,
                          gene_18138, gene_18811, gene_19805)

LASSO_trainData <- DATOS_LASSO[1:640,]
LASSO_testData <- DATOS_LASSO[641:801,]
```


```{r}
modelo_NB_LASSO <- naive_bayes(Cáncer~., data = LASSO_trainData)
modelo_NB_LASSO$tables
```

```{r}
summary(modelo_NB_LASSO)
```


```{r}
pred_NB_LASSO <- predict(modelo_NB_LASSO, LASSO_testData)
head(pred_NB_LASSO)
```

```{r}
matriz_NB_LASSO <- confusionMatrix(as.factor(LASSO_testData$Cáncer), as.factor(pred_NB_LASSO))
matriz_NB_LASSO
```

CONCLUSIÓN: EN ESTE CASO LAS VARIABLES PREDICTORAS FUNCIONAN CORRECTAMENTE



# MODELO BAYES CON BIOCONDUCTOR

```{r}
library(BiocManager)
library(limma)
library(edgeR)
library(DESeq2)
```

```{r}
BiocManager::install("RegParallel")
library(RegParallel)
```

####VARIABLES PREDICTORAS AIC

```{r}
bayes_bio_AIC <- bayesglm(var_AIC$Cáncer_COAD~., data = var_AIC)
summary(bayes_bio_AIC)
```


```{r}
pred_bio_AIC <- as.numeric(predict(bayes_bio_AIC, var_AIC_X_test, type = 'response')<.5)
pred_bio_AIC
```

```{r}
matriz_bio_AIC <- confusionMatrix(as.factor((var_AIC_Y_test)), as.factor((pred_bio_AIC)), 
                                  mode = 'everything')
matriz_bio_AIC
```

```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_AIC), confint.default(bayes_bio_AIC, level = 0.99)))
```

```{r}
coef_bio_AIC_COAD801 <- as.data.frame(bayes_bio_AIC$coefficients)
coef_bio_AIC_COAD801 <- (coef_bio_AIC_COAD801[2:9,1])

exportar_bio_AIC <- cbind(genes_AIC, coef_bio_AIC_COAD801)

write.csv(exportar_bio_AIC, 'coef_bio_AIC_COAD801.csv')
```


####VARIABLES PREDICTORAS OVER

```{r}
bayes_bio_OVER <- bayesglm(var_overf$Cáncer_COAD~., data = var_overf)
summary(bayes_bio_OVER)
```

```{r}
pred_bio_OVER <- as.numeric(predict(bayes_bio_OVER, var_overf_X_test, type = 'response')<.5)
pred_bio_OVER
```

```{r}
matriz_bio_OVER <- confusionMatrix(as.factor((var_overf_Y_test)), as.factor((pred_bio_OVER)), 
                                  mode = 'everything')
matriz_bio_OVER
```


```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_OVER), confint.default(bayes_bio_OVER, level = 0.99)))
```

```{r}
coef_bio_OVER_COAD801 <- as.data.frame(bayes_bio_OVER$coefficients)
coef_bio_OVER_COAD801 <- (coef_bio_OVER_COAD801[2:10,1])

exportar_bio_OVER <- cbind(genes_OVER, coef_bio_OVER_COAD801)

write.csv(exportar_bio_OVER, 'coef_bio_OVER_COAD801.csv')
```


####VARIABLES PREDICTORAS TRES

```{r}
bayes_bio_TRES <- bayesglm(var_overf_tres$Cáncer_COAD~., data = var_overf_tres)
summary(bayes_bio_TRES)
```

```{r}
pred_bio_TRES <- as.numeric(predict(bayes_bio_TRES, tres_overf_X_test, type = 'response')<.5)
pred_bio_TRES
```

```{r}
matriz_bio_TRES <- confusionMatrix(as.factor((tres_overf_Y_test)), as.factor((pred_bio_TRES)), 
                                  mode = 'everything')
matriz_bio_TRES
```


```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_TRES), confint.default(bayes_bio_TRES, level = 0.99)))
```

```{r}
coef_bio_TRES_COAD801 <- as.data.frame(bayes_bio_TRES$coefficients)
coef_bio_TRES_COAD801 <- (coef_bio_TRES_COAD801[2:9,1])

exportar_bio_TRES <- cbind(genes_TRES, coef_bio_TRES_COAD801)

write.csv(exportar_bio_TRES, 'coef_bio_TRES_COAD801.csv')
```


#### VARIABLES PREDICTORAS LASSO

```{r}
bayes_bio_LASSO <- bayesglm(var_LASSO$Cáncer_COAD~., data = var_LASSO)
summary(bayes_bio_LASSO)
```

```{r}
pred_bio_LASSO <- as.numeric(predict(bayes_bio_LASSO, LASSO_X_test, type = 'response')<.5)
pred_bio_LASSO
```

```{r}
matriz_bio_LASSO <- confusionMatrix(as.factor((LASSO_Y_test)), as.factor((pred_bio_LASSO)), 
                                  mode = 'everything')
matriz_bio_LASSO
```

```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_LASSO), confint.default(bayes_bio_LASSO, level = 0.99)))
```

```{r}
coef_bio_LASSO_COAD801 <- as.data.frame(bayes_bio_LASSO$coefficients)
coef_bio_LASSO_COAD801 <- (coef_bio_LASSO_COAD801[2:36,1])

exportar_bio_LASSO <- cbind(genes_LASSO, coef_bio_LASSO_COAD801)

write.csv(exportar_bio_LASSO, 'coef_bio_LASSO_COAD801.csv')
```



# ENTRENAMIENTO ALGORITMO GENÉTICO

```{r}
library(mlbench)
library(dplyr)
```

```{r}
library(caret)
library(randomForest)
library(funModeling)
library(tidyverse)
library(GA)
```

#### VARIABLES PREDICTORAS AIC

```{r}
model_ga_AIC <- gafs(x = AIC_trainData[, -1], 
                 y = as.factor(AIC_trainData$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga_AIC
```

#### VARIABLES PREDICTORAS OVER

```{r}
model_ga_OVER <- gafs(x = OVER_trainData[, -1], 
                 y = as.factor(OVER_trainData$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga_OVER
```

#### VARIABLES PREDICTORAS LASSO

```{r}
model_ga_LASSO <- gafs(x = LASSO_trainData[, -1], 
                 y = as.factor(LASSO_trainData$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga_LASSO
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
