---
title: "Análisis del conjunto completo de datos, 801 muestras"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

# CARGA DE BIBLIOTECAS Y DATOS

#### Instalación de todos los paquetes que vamos a utilizar durante el análisis.

```{r}
list.of.packages <- c("ggplot2", "dplyr", "fastDummies", "faraway", "tidyverse", "rlang", "cowplot", "caret", "lattice",
                      "e1071", "glmnet", "Matrix", "caretEnsemble", "GGally", "rpart", "randomForest", "naivebayes",
                      "BiocManager", "MLmetrics", "mlbench", "parallel", "doParallel", "funModeling", "GA", "corrplot")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

```{r}
library(GGally)
library(fastDummies)
library(ggplot2)
library(dplyr)
library(faraway)
library(tidyverse)
library(rlang)
library(cowplot)
library(corrplot)
```


#### Carga de los datos a analizar

```{r}
datos_TOTALES <- read.csv('C:\\Users\\Laura\\Desktop\\TFM Total muestras\\Totales\\datos_totales.csv', sep = ',', encoding = 'UTF-8')

datos_TOTALES$Muestra <- NULL
head(datos_TOTALES)

datos_TOTALES_mat <- as.matrix(datos_TOTALES)
```

```{r}
summary(datos_TOTALES)
```


# REPRESENTACIÓN GRÁFICA DE LOS DATOS

#### Conjunto de los datos

```{r}
ggplot(datos_TOTALES, aes(x = Cáncer, fill = Cáncer) ) + geom_bar() +
  scale_fill_manual(values = c('#6897bb', '#283655', '#4d648d', '#d0e1f9', '#1e1f26')) + theme_bw()
```

#### Expresión génica en cajas

```{r}
plot_for_loop <- function(df, .x_var, .y_var) {
  
  # convert strings to variable
  x_var <- sym(.x_var)
  y_var <- sym(.y_var)
  
  # unquote variables using !! 
  ggplot(df, aes(x= !! x_var, y = !! y_var, fill = !! x_var)) + 
    geom_boxplot() + 
    labs(x = x_var, y = y_var) +
    theme(axis.text = element_text(size = 10), axis.title = element_text(size = 15), legend.key.size = unit(1, 'cm')) +
    scale_fill_manual(values = c('#6897bb', '#283655', '#4d648d', '#d0e1f9', '#1e1f26')) + 
    theme_bw()
}
```

```{r}
plot_list <- colnames(datos_TOTALES)[-1] %>% 
  map( ~ plot_for_loop(datos_TOTALES, colnames(datos_TOTALES)[1], .x))
```

```{r}
pdf('plot_list_Sel801.pdf', height = 10, width = 10)

plot_list

dev.off()
```


# ESTUDIO PREVIO DE VARIABLES PREDICTORAS

#### Creación de variables DUMMYS

```{r}
genes_TOTALES <- (datos_TOTALES[,2:98])
```

```{r}
relacion_gen <- cor(genes_TOTALES)
print(relacion_gen)
```

```{r}
corrplot(cor(genes_TOTALES), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .3, cl.cex = .8)
```

```{r}
cor_muestras_FPC <- cor(genes_TOTALES)
ord <- corrMatOrder(cor_muestras_FPC, order="FPC")
cor_muestras_FPC_dibujo <- cor_muestras_FPC[ord, ord]

pdf('plot_corr_muestras_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_muestras_FPC_dibujo, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = .5, cl.cex = 5)

dev.off()
```



# MODELO NAIVE BAYES

```{r}
library(caretEnsemble)
library(GGally)
library(rpart)
library(randomForest)
library(naivebayes)
```

```{r}
datos_x <- (datos_TOTALES[1:640,])
datos_y <- (datos_TOTALES[641:801,])
```


```{r}
modelo_NB <- naive_bayes(Cáncer~., data = datos_x)
modelo_NB
```
```{r}
modelo_NB$tables
```

```{r}
summary(modelo_NB)
```


```{r}
pred_NB <-(predict(modelo_NB, datos_y))
head(pred_NB)
```
```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
matriz_NB <- confusionMatrix(as.factor(datos_y$Cáncer), as.factor(pred_NB), mode = 'everything')
matriz_NB
```



# ENTRENAMIENTO ALGORITMO GENÉTICO

```{r}
library(mlbench)
library(dplyr)
```

```{r}
library(caret)
library(randomForest)
library(funModeling)
library(tidyverse)
library(GA)
```


```{r}
model_ga <- gafs(x = datos_x [, -1], 
                 y = as.factor(datos_x$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga
```

```{r}
genes_ga <- select(datos_TOTALES, Cáncer, gene_10000, gene_10301, gene_11463, gene_12225, gene_15009)
write.csv(genes_ga, 'genes_ga.csv')
```

```{r}
predict_ga_test <- predict.gafs(model_ga,datos_y)
predict_ga_test
```

```{r}
write.csv(predict_ga_test, file = 'predict_ga_test.csv', row.names = FALSE)
```


```{r}
predict_ga_train <- predict.gafs(model_ga, datos_x)
predict_ga_train
```

```{r}
write.csv(predict_ga_train, file = 'predict_ga_train.csv', row.names = FALSE)
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
