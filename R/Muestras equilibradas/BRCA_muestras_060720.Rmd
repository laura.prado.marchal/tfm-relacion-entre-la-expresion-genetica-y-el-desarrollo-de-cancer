---
title: "Análisis BRCA con selección de muestras"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

# CARGA DE BIBLIOTECAS Y DATOS

#### Instalación de todos los paquetes que vamos a utilizar durante el análisis.

```{r}
list.of.packages <- c("ggplot2", "dplyr", "fastDummies", "faraway", "tidyverse", "rlang", "cowplot", "caret", "lattice",
                      "e1071", "glmnet", "Matrix", "caretEnsemble", "GGally", "rpart", "randomForest", "naivebayes",
                      "BiocManager", "MLmetrics", "mlbench", "parallel", "doParallel", "funModeling", "GA", "corrplot")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

```{r}
library(GGally)
library(fastDummies)
library(ggplot2)
library(dplyr)
library(faraway)
library(tidyverse)
library(rlang)
library(cowplot)
library(corrplot)
```


#### Carga de los datos a analizar

```{r}
datos_BRCA <- read.csv('C:\\Users\\Laura\\Desktop\\TFM Selec Muestras\\BRCA\\datos_BRCA_muestras.csv', sep = ',', encoding = 'UTF-8')

datos_BRCA$Muestra <- NULL
head(datos_BRCA)

datos_BRCA_mat <- as.matrix(datos_BRCA)
```

```{r}
summary(datos_BRCA)
```


# REPRESENTACIÓN GRÁFICA DE LOS DATOS

#### Conjunto de los datos

```{r}
ggplot(datos_BRCA, aes(x = Cáncer, fill = Cáncer) ) + geom_bar() + scale_fill_manual(values = c('#4d648d', '#d0e1f9'))
```

#### Expresión génica en cajas

```{r}
plot_for_loop <- function(df, .x_var, .y_var) {
  
  # convert strings to variable
  x_var <- sym(.x_var)
  y_var <- sym(.y_var)
  
  # unquote variables using !! 
  ggplot(df, aes(x= !! x_var, y = !! y_var)) + 
    geom_boxplot() + 
    labs(x = x_var, y = y_var) +
    theme(axis.text = element_text(size = 10), axis.title = element_text(size = 15), legend.key.size = unit(1, 'cm')) +
    scale_fill_manual(values = c('#4d648d', '#d0e1f9'))
}
```

```{r}
plot_list <- colnames(datos_BRCA)[-1] %>% 
  map( ~ plot_for_loop(datos_BRCA, colnames(datos_BRCA)[1], .x))
```

```{r}
pdf('plot_list_SelMuestras.pdf', height = 10, width = 10)

plot_list

dev.off()
```


# ESTUDIO PREVIO DE VARIABLES PREDICTORAS

#### Creación de variables DUMMYS

```{r}
datos_BRCA_dum <- dummy_cols(datos_BRCA, select_columns = c('Cáncer'))

datos_BRCA_dum$Cáncer <- NULL
datos_BRCA_dum$Cáncer_Otro <- NULL

datos_BRCA_dum <- select(datos_BRCA_dum, Cáncer_BRCA, everything())
head(datos_BRCA_dum)

BRCA_genes <- select(datos_BRCA_dum, -(Cáncer_BRCA))
```

#### Estudio correlación

Análisis de la correlación en tabla

```{r}
relacion_gen <- cor(BRCA_genes)
print(relacion_gen)
```

```{r}
corrplot(cor(BRCA_genes), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .3, cl.cex = .8)
```

```{r}
cor_muestras_FPC <- cor(BRCA_genes)
ord <- corrMatOrder(cor_muestras_FPC, order="FPC")
cor_muestras_FPC_dibujo <- cor_muestras_FPC[ord, ord]

pdf('plot_corr_muestras_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_muestras_FPC_dibujo, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = .5, cl.cex = 5)

dev.off()
```



# SELECCIÓN DE VARIABLES PREDICTORAS.

#### Seleción stepAIC

```{r}
fit1 <- glm(Cáncer_BRCA~., data = datos_BRCA_dum, family = 'binomial')
fit0 <- glm(Cáncer_BRCA~1, data = datos_BRCA_dum, family = 'binomial')
```

```{r}
install.packages("MASS")
library(MASS)

step_both <- stepAIC (fit0, direction = 'both', scope=list(upper=fit1, lower=fit0))
```

```{r}
summary(step_both)
```


```{r}
coefficients(step_both)
```

```{r}
BRCA_X <- (datos_BRCA_dum[,2:99])

BRCA_Y <- (datos_BRCA_dum[,1])
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
y_predicc_both <- as.numeric(predict(step_both, BRCA_X, type = 'response')>.5)

head(y_predicc_both)
```

```{r}
matriz_STEPWISE <- confusionMatrix(as.factor(BRCA_Y), as.factor(y_predicc_both), mode = 'everything')

matriz_STEPWISE
```

Estas métricas nos hacen intuir que existe colinealidad, para comprobarlo se repite el proceso sobre train y test

```{r}
BRCA_X_train <- datos_BRCA_dum[1:234, 2:99]
BRCA_X_test <- datos_BRCA_dum[235:390, 2:99]

BRCA_Y_train <- datos_BRCA_dum[1:234, 1]
BRCA_Y_test <- datos_BRCA_dum[235:390, 1]
```

```{r}
y_predicc_train <- as.numeric(predict(step_both, BRCA_X_train, type = 'response')>.5)

matriz_STEPWISE_train <- confusionMatrix(as.factor(BRCA_Y_train), as.factor(y_predicc_train), mode = 'everything')

matriz_STEPWISE_train
```

```{r}
y_predicc_test <- as.numeric(predict(step_both, BRCA_X_test, type = 'response')>.5)

matriz_STEPWISE_test <- confusionMatrix(as.factor(BRCA_Y_test), as.factor(y_predicc_test), mode = 'everything')

matriz_STEPWISE_test
```

CONCLUSIÓN: LOS DATOS OBTENIDOS DE LA MATRIZ PARA TRAIN Y TEST NOS INDICAN LA EXISTENCIA DE OVERFITTING, PARA EVITARLA PROBAREMOS CON TRES TIPOS DE SELECCIÓN DE VARIABLES: 1. LA OBTENIDA POR EL MENOR AIC, 2. LA OBTENIDA POR EL SEGUNDO MENOR AIC Y 3. LA OBTENIDA POR LASSO.


#### SELECCIÓN CON LASSO

```{r}
BRCA_X_mat <- data.matrix(datos_BRCA_dum[,2:99])

BRCA_Y_mat <- data.matrix(datos_BRCA_dum[,1])

library(glmnet)

set.seed(999)

BRCA_selec_lasso <- cv.glmnet(BRCA_X_mat, BRCA_Y_mat, family = 'binomial', alpha = 1, parallel = TRUE, standardize = TRUE,
                              type.measure = 'auc')
plot(BRCA_selec_lasso)
```

```{r}
BRCA_selec_lasso$lambda.min
```

```{r}
coef(BRCA_selec_lasso, s = BRCA_selec_lasso$lambda.min)
```

```{r}
library(caret)
library(lattice)
library(e1071)
```


```{r}
y_selec_LASSO <- as.numeric(predict.glmnet(BRCA_selec_lasso$glmnet.fit, newx = BRCA_X_mat, s =
                                             BRCA_selec_lasso$lambda.min)>.5)

(y_selec_LASSO)
```

```{r}
matrix_selec_LASSO <- confusionMatrix(as.factor((BRCA_Y_mat)), as.factor((y_selec_LASSO)), 
                                      mode = 'everything')
matrix_selec_LASSO
```



# CREACIÓN DE SUBCONJUNTOS

#### VARIABLES PREDICTORAS AIC

```{r}
detach(package: MASS)

variables_AIC <- select(datos_BRCA_dum, Cáncer_BRCA, gene_13436, gene_15380, gene_1503, gene_3543, gene_7633, gene_4292,
                        gene_19368, gene_10530, gene_11463, gene_4053)

variables_AIC_X_train <- variables_AIC [1:234, 2:11]
variables_AIC_X_test <- variables_AIC [235:390, 2:11]

variables_AIC_Y_train <- variables_AIC [1:234, 1]
variables_AIC_Y_test <- variables_AIC [235:390, 1]

variables_AIC_cor <- select(variables_AIC, -(Cáncer_BRCA))
```

```{r}
corrplot(cor(variables_AIC_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .6, cl.cex = .8)
```

```{r}
cor_muestra_AIC_FPC <- cor(variables_AIC_cor)
ord <- corrMatOrder(cor_muestra_AIC_FPC, order="FPC")
cor_dibujo_AIC_FPC <- cor_muestra_AIC_FPC[ord, ord]

pdf('plot_muestra_AIC_FPC_dos.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_AIC_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 1, cl.cex = 5)

dev.off()
```

```{r}
write.csv(variables_AIC, file = 'variables_AIC_BRCA390.csv', row.names = FALSE)
```


#### VARIABLES SIN COLINEALIDAD

```{r}
var_OVER <- select(datos_BRCA_dum, Cáncer_BRCA, gene_13436, gene_15380, gene_1503, gene_3543, gene_7633, gene_4292,
                    gene_19368, gene_10530, gene_11463)

var_OVER_X_train <- (var_OVER [1:234, 2:10])
var_OVER_X_test <- (var_OVER [235:390, 2:10])

var_OVER_Y_train <- (var_OVER [1:234,1])
var_OVER_Y_test <- (var_OVER [235:390, 1])

var_OVER_cor <- select(var_OVER, -(Cáncer_BRCA))
```

```{r}
corrplot(cor(var_OVER_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .7, cl.cex = .8)
```


```{r}
cor_muestra_OVER_FPC <- cor(var_OVER_cor)
ord <- corrMatOrder(cor_muestra_OVER_FPC, order="FPC")
cor_dibujo_OVER_FPC <- cor_muestra_OVER_FPC[ord, ord]

pdf('plot_muestra_OVER_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_OVER_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 2, cl.cex = 5)

dev.off()
```

```{r}
write.csv(var_OVER, file = 'variables_OVER_BRCA390.csv', row.names = FALSE)
```


#### VARIABLES LASSO

```{r}
var_LASSO <- select(datos_BRCA_dum, Cáncer_BRCA, gene_125, gene_340, gene_346, gene_595, gene_682, gene_1503, gene_1762,
                    gene_1865, gene_3436, gene_3509, gene_3543, gene_3546, gene_4053, gene_4292, gene_4746, gene_7551,
                    gene_7633, gene_8180, gene_10301, gene_10530, gene_11166, gene_11463, gene_11704, gene_12225, 
                    gene_12300, gene_12843, gene_13436, gene_13479, gene_13977, gene_14112, gene_14681, gene_15009,
                    gene_15380, gene_15652, gene_16359, gene_17580, gene_17691, gene_18138, gene_18562, gene_18669,
                    gene_19293, gene_19765)   

var_LASSO_X_train <- data.matrix(var_LASSO [1:234, 2:43])
LASSO_X_train <- (var_LASSO [1:234, 2:43])
var_LASSO_X_test <- data.matrix(var_LASSO [235:390, 2:43])
LASSO_X_test <- (var_LASSO [235:390, 2:43])

var_LASSO_Y_train <- data.matrix(var_LASSO [1:234,1])
LASSO_Y_train <- (var_LASSO [1:234,1])
var_LASSO_Y_test <- data.matrix(var_LASSO [235:390, 1])
LASSO_Y_test <- (var_LASSO [235:390, 1])

var_LASSO_cor <- select(var_LASSO, -(Cáncer_BRCA))
```

```{r}
corrplot(cor(var_LASSO_cor), method = "color", outline = T, addgrid.col = "darkgray",
         order="FPC", cl.pos = "r", tl.col =
           "black", tl.cex = .6, cl.cex = .8)
```


```{r}
cor_muestra_LASSO_FPC <- cor(var_LASSO_cor)
ord <- corrMatOrder(cor_muestra_LASSO_FPC, order="FPC")
cor_dibujo_LASSO_FPC <- cor_muestra_LASSO_FPC[ord, ord]

pdf('plot_muestra_LASSO_FPC.pdf', height = 50, width = 50)

corrplot.mixed(cor_dibujo_LASSO_FPC, lower = 'number', upper = 'circle', tl.pos = 'd',
               addgrid.col = 'darkgrey', tl.cex = 2, cl.cex = 5)

dev.off()
```

```{r}
write.csv(var_LASSO, file = 'variables_LASSO_BRCA390.csv', row.names = FALSE)
```



# ENTRENAMIENTO DE REGRESIÓN LOGÍSTICA

#### VARIABLES AIC

```{r}
mod_STEPWISE <- glm(Cáncer_BRCA~., data = variables_AIC, family = 'binomial')
```

```{r}
summary(mod_STEPWISE)
```

```{r}
y_pred_STEPWISE <- as.numeric(predict(mod_STEPWISE, variables_AIC_X_test, type = 'response')> .5)
head(y_pred_STEPWISE)
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
matrix_STEPWISE <- confusionMatrix(as.factor(y_pred_STEPWISE), as.factor(variables_AIC_Y_test), mode = 'everything')
matrix_STEPWISE
```

```{r}
coef(mod_STEPWISE)
```

```{r}
exp(coef(mod_STEPWISE))
```

```{r}
exp(confint(mod_STEPWISE))
```

```{r}
coef_AIC_BRCA390 <- as.data.frame(mod_STEPWISE$coefficients)
coef_AIC_BRCA390 <- (coef_AIC_BRCA390[2:11,1])

genes_AIC <- data.frame(t(variables_AIC[-1]))

exportar_AIC <- cbind(genes_AIC, coef_AIC_BRCA390)
exportar_AIC <- select(exportar_AIC, (coef_AIC_BRCA390))

write.csv(exportar_AIC, 'coef_AIC_BRCA390.csv')
```

CONCLUSIÓN: LAS VARIABLES PRESENTA COLINEALIDAD POR ESTE MOTIVO NINGUNA DE ELLAS PRESENTA UN p-value SIGNIFICATIVO


#### VARIABLES CON SEGUNDO MENOR AIC

```{r}
mod_var_over <- glm(Cáncer_BRCA~., data = var_OVER, family = 'binomial')
```

```{r}
summary(mod_var_over)
```

```{r}
y_pred_over <- as.numeric(predict(mod_var_over, var_OVER_X_test, type = 'response')> .5)
head(y_pred_over)
```

```{r}
library(caret)
library(lattice)
library(e1071)
```

```{r}
matrix_over <- confusionMatrix(as.factor(y_pred_over), as.factor(var_OVER_Y_test), mode = 'everything')
matrix_over
```

EN ESTE CASO PARECE QUE DESAPARECE EL OVERFITTING

```{r}
coef(mod_var_over)
```

```{r}
exp(coef(mod_var_over))
```

```{r}
exp(confint(mod_var_over))
```

```{r}
coef_OVER_BRCA390 <- as.data.frame(mod_var_over$coefficients)
coef_OVER_BRCA390 <- (coef_OVER_BRCA390[2:10,1])

genes_OVER <- data.frame(t(var_OVER[-1]))

exportar_OVER <- cbind(genes_OVER, coef_OVER_BRCA390)
exportar_OVER <- select(exportar_OVER, (coef_OVER_BRCA390))

write.csv(exportar_OVER, 'coef_OVER_BRCA390.csv')
```


#### VARIABLES PREDICTORAS LASSO

```{r}
logit_LASSO <- cv.glmnet(var_LASSO_X_train, var_LASSO_Y_train, family = 'binomial', alpha = 1, 
                         parallel =TRUE,
                         standardize = TRUE, type.measure = 'auc')

plot(logit_LASSO)
```

```{r}
logit_LASSO$lambda.min
```


```{r}
y_predicc_LASSO <- as.numeric(predict.glmnet(logit_LASSO$glmnet.fit, newx = var_LASSO_X_test, s =
                                               logit_LASSO$lambda.min)>.5)

y_predicc_LASSO
```

```{r}
matrix_LASSO <- confusionMatrix(as.factor(var_LASSO_Y_test), as.factor(y_predicc_LASSO), mode = 'everything')
matrix_LASSO
```

```{r}
logit_LASSO$lambda.min
coef(logit_LASSO, s = logit_LASSO$lambda.min)
```


```{r}
coef_LASSO_BRCA390<- as.numeric(coef(logit_LASSO, s = logit_LASSO$lambda.min))
coef_LASSO_BRCA390 <- as.data.frame(coef_LASSO_BRCA390)
coef_LASSO_BRCA390 <- (coef_LASSO_BRCA390[2:43,1])

genes_LASSO <- data.frame(t(var_LASSO[-1]))

exportar_LASSO <- cbind(genes_LASSO, coef_LASSO_BRCA390)
exportar_LASSO <- select(exportar_LASSO, (coef_LASSO_BRCA390))

write.csv(exportar_LASSO, 'coef_LASSO_BRCA390.csv')
```

CONCLUSIÓN: EL MODELO DE REGRESIÓN LOGÍSTICA PRESENTA COLINEALIAD CUANDO UTILIZAMOS LA SELECCIÓN DE VARIABLES PREDICTORAS OFRECIDAS POR stepAIC. SIN EMBARGO, CUANDO SE UTILIZA EL SEGUNDO MENOR AIC O LA SELECCIÓN DE LASSO EL MODELO TIENE UNA ALTA EFICACIA



# MODELO NAIVE BAYES

```{r}
library(caretEnsemble)
library(GGally)
library(rpart)
library(randomForest)
library(naivebayes)
```

#### VARIABLES PREDICTARAS AIC

```{r}
DATOS_AIC <- select(datos_BRCA, Cáncer, gene_13436, gene_15380, gene_1503, gene_3543, gene_7633, gene_4292,
                        gene_19368, gene_10530, gene_11463, gene_4053)

AIC_trainData <- DATOS_AIC[1:234,]
AIC_testData <- DATOS_AIC[235:390,]
```


```{r}
modelo_NB_AIC <- naive_bayes(Cáncer~., data = AIC_trainData)
modelo_NB_AIC$tables
```

```{r}
summary(modelo_NB_AIC)
```


```{r}
pred_NB <- predict(modelo_NB_AIC, AIC_testData)
head(pred_NB)
```

```{r}
matriz_NB_AIC <- confusionMatrix(as.factor(AIC_testData$Cáncer), as.factor(pred_NB), mode='everything')
matriz_NB_AIC
```

#### VARIABLES PREDICTORAS OVER

```{r}
datos_over <- select(datos_BRCA, Cáncer, gene_13436, gene_15380, gene_1503, gene_3543, gene_7633, gene_4292,
                    gene_19368, gene_10530, gene_11463)

OVER_trainData <- datos_over[1:234,]
OVER_testData <- datos_over[235:390,]
```


```{r}
modelo_NB_OVER <- naive_bayes(Cáncer~., data = OVER_trainData)
modelo_NB_OVER$tables
```

```{r}
summary(modelo_NB_OVER)
```

```{r}
pred_NB_OVER <- predict(modelo_NB_OVER, OVER_testData)
head(pred_NB_OVER)
```

```{r}
matriz_NB_OVER <- confusionMatrix(as.factor(OVER_testData$Cáncer), as.factor(pred_NB_OVER), mode = 'everything')
matriz_NB_OVER
```


#### VARIABLES PREDICTORAS LASSO

```{r}
DATOS_LASSO <- select(datos_BRCA, Cáncer,gene_125, gene_340, gene_346, gene_595, gene_682, gene_1503, gene_1762,
                    gene_1865, gene_3436, gene_3509, gene_3543, gene_3546, gene_4053, gene_4292, gene_4746, gene_7551,
                    gene_7633, gene_8180, gene_10301, gene_10530, gene_11166, gene_11463, gene_11704, gene_12225, 
                    gene_12300, gene_12843, gene_13436, gene_13479, gene_13977, gene_14112, gene_14681, gene_15009,
                    gene_15380, gene_15652, gene_16359, gene_17580, gene_17691, gene_18138, gene_18562, gene_18669,
                    gene_19293, gene_19765)

LASSO_trainData <- DATOS_LASSO[1:234,]
LASSO_testData <- DATOS_LASSO[235:390,]
```


```{r}
modelo_NB_LASSO <- naive_bayes(Cáncer~., data = LASSO_trainData)
modelo_NB_LASSO$tables
```

```{r}
summary(modelo_NB_LASSO)
```

```{r}
pred_NB_LASSO <- predict(modelo_NB_LASSO, LASSO_testData)
head(pred_NB_LASSO)
```

```{r}
matriz_NB_LASSO <- confusionMatrix(as.factor(LASSO_testData$Cáncer), as.factor(pred_NB_LASSO), mode = 'everything')
matriz_NB_LASSO
```

CONCLUSIÓN: EN ESTE CASO LAS VARIABLES PREDICTORAS FUNCIONAN CORRECTAMENTE



# MODELO BAYES CON BIOCONDUCTOR

```{r}
library(BiocManager)
library(limma)
library(edgeR)
library(DESeq2)
```

```{r}
BiocManager::install("RegParallel")
library(RegParallel)
```

####VARIABLES PREDICTORAS AIC

```{r}
bayes_bio_AIC <- bayesglm(variables_AIC$Cáncer_BRCA~., data = variables_AIC)
summary(bayes_bio_AIC)
```

```{r}
pred_bio_AIC <- as.numeric(predict(bayes_bio_AIC, variables_AIC_X_test, type = 'response')<.5)
head(pred_bio_AIC)
```

```{r}
matriz_bio_AIC <- confusionMatrix(as.factor((variables_AIC_Y_test)), as.factor((pred_bio_AIC)), mode = 'everything')
matriz_bio_AIC
```

```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_AIC), confint.default(bayes_bio_AIC, level = 0.99)))
```

```{r}
coef_bio_AIC_BRCA390 <- as.data.frame(bayes_bio_AIC$coefficients)
coef_bio_AIC_BRCA390 <- (coef_bio_AIC_BRCA390[2:11,1])

exportar_bio_AIC <- cbind(genes_AIC, coef_bio_AIC_BRCA390)

write.csv(exportar_bio_AIC, 'coef_bio_AIC_BRCA390.csv')
```


####VARIABLES PREDICTORAS OVER

```{r}
bayes_bio_OVER <- bayesglm(var_OVER$Cáncer_BRCA~., data = var_OVER)
summary(bayes_bio_OVER)
```

```{r}
pred_bio_OVER <- as.numeric(predict(bayes_bio_OVER, var_OVER_X_test, type = 'response')<.5)
pred_bio_OVER
```

```{r}
matriz_bio_OVER <- confusionMatrix(as.factor((var_OVER_Y_test)), as.factor((pred_bio_OVER)), 
                                   mode = 'everything')
matriz_bio_OVER
```

```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_OVER), confint.default(bayes_bio_OVER, level = 0.99)))
```


```{r}
coef_bio_OVER_BRCA390 <- as.data.frame(bayes_bio_OVER$coefficients)
coef_bio_OVER_BRCA390 <- (coef_bio_OVER_BRCA390[2:10,1])

exportar_bio_OVER <- cbind(genes_OVER, coef_bio_OVER_BRCA390)

write.csv(exportar_bio_OVER, 'coef_bio_OVER_BRCA390.csv')
```


#### VARIABLES PREDICTORAS LASSO

```{r}
bayes_bio_LASSO <- bayesglm(var_LASSO$Cáncer_BRCA~., data = var_LASSO)
summary(bayes_bio_LASSO)
```

```{r}
pred_bio_LASSO <- as.numeric(predict(bayes_bio_LASSO, LASSO_X_test, type = 'response')<.5)
pred_bio_LASSO
```

```{r}
exp(cbind("Odds ratio" = coef(bayes_bio_LASSO), confint.default(bayes_bio_LASSO, level = 0.99)))
```

```{r}
matriz_bio_LASSO <- confusionMatrix(as.factor(LASSO_Y_test), as.factor(pred_bio_LASSO), mode = 'everything')
matriz_bio_LASSO
```


```{r}
coef_bio_LASSO_BRCA390 <- as.data.frame(bayes_bio_LASSO$coefficients)
coef_bio_LASSO_BRCA390 <- (coef_bio_LASSO_BRCA390[2:43,1])

exportar_bio_LASSO <- cbind(genes_LASSO, coef_bio_LASSO_BRCA390)

write.csv(exportar_bio_LASSO, 'coef_bio_LASSO_BRCA390.csv')
```



# ENTRENAMIENTO ALGORITMO GENÉTICO

```{r}
library(mlbench)
library(dplyr)
```

```{r}
library(caret)
library(randomForest)
library(funModeling)
library(tidyverse)
library(GA)
```

#### VARIABLES PREDICTORAS AIC

```{r}
model_ga_AIC <- gafs(x = AIC_trainData[, -1], 
                 y = as.factor(AIC_trainData$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga_AIC
```

#### VARIABLES PREDICTORAS OVER

```{r}
model_ga_OVER <- gafs(x = OVER_trainData[, -1], 
                 y = as.factor(OVER_trainData$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga_OVER
```



#### VARIABLES PREDICTORAS LASSO

```{r}
model_ga_LASSO <- gafs(x = LASSO_trainData[, -1], 
                 y = as.factor(LASSO_trainData$Cáncer),
                 iters = 10, # generations of algorithm
                 popSize = 10, # population size for each generation
                 levels = c("malignant", "benign"),
                 gafsControl = gafsControl(functions = rfGA, # Assess fitness with RF
                                           method = "cv",    # 10 fold cross validation
                                           genParallel = TRUE, # Use parallel programming
                                           allowParallel = TRUE))
```

```{r}
model_ga_LASSO
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
